require 'erb'
require 'securerandom'

# configuration
require './cluster-config'

MASTER_DNS = "#{CLUSTER_NAME}-master"
WORKER_DNS_TEMPLATE = Proc.new { |i| "#{CLUSTER_NAME}-worker-#{i}" }

shared_secret = SHARED_SECRET
print_shared_secret_message = false

if shared_secret == nil then
    shared_secret = SecureRandom.hex
    print_shared_secret_message = true
end

# render config.toml.tmpl
template = ERB.new(IO.read("config.toml.tmpl.erb"))
IO.write("config.toml.tmpl", template.result(binding))

Vagrant.configure("2") do |config|
    config.vm.box = "generic/alpine39"
    config.vm.guest = :alpine
    config.ssh.forward_agent = true
    
    config.vm.provider "hyperv" do |hv|
        hv.cpus = 2
        hv.memory = 256
        hv.maxmemory = 20480
        hv.linked_clone = true
    end

    config.trigger.before :up do |t|
        t.ruby do |env, machine|
            # can't actually generate shared_secret here bc the other configs
            # which use it are committed to the value of shared_secret earlier
            # in execution
            if print_shared_secret_message then
                puts "generating new cluster shared secret: #{shared_secret}"
                print_shared_secret_message = false
            end
        end
    end

    config.vm.define "master" do |master|
        master.vm.hostname = MASTER_DNS

        # update containerd config to allow a plain-HTTP local registry
        master.vm.provision "file", source: "config.toml.tmpl",
            destination: "config.toml.tmpl"
        # move preloaded manifests into the VM
        master.vm.provision "file", source: "./manifests",
            destination: "manifests"

        master.vm.provision "shell", path: "install-k3s-master.sh",
            reset: true, args: [ shared_secret, "#{NETWORK}.#{MASTER}",
            "#{NETWORK}.#{GATEWAY}", TIMEZONE ]
    end

    (1..N).each do |i|
        config.vm.define "worker-#{i}" do |worker|
            worker.vm.hostname = WORKER_DNS_TEMPLATE.call(i)

            # update containerd config to allow a plain-HTTP local registry
            worker.vm.provision "file", source: "config.toml.tmpl",
                destination: "config.toml.tmpl"

            worker.vm.provision "shell", path: "install-k3s-worker.sh",
                reset: true, args: [ shared_secret, "#{NETWORK}.#{WORKER + i}",
                "#{NETWORK}.#{GATEWAY}", TIMEZONE, "#{NETWORK}.#{MASTER}",
                MASTER_DNS ]
        end
    end
end
