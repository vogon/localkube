N = 2
SHARED_SECRET = nil     # set to something else to e.g. restart failed nodes in
                        # an existing cluster
NETWORK = "192.168.200"
GATEWAY = 1
MASTER = 64
WORKER = 127
CLUSTER_NAME = "mozak"
TIMEZONE = "US/Pacific" # Hyper-V can't emulate a UTC real-time clock so I guess
                        # we have to live with that now
