#!/bin/bash

SHARED_SECRET=$1
THIS_NODE_IP=$2
GATEWAY_IP=$3
TIMEZONE=$4
MASTER_IP=$5
MASTER_DNS=$6

echo "hello from a worker node"

# reconfigure eth0 to route through the host computer
# TODO: allow an arbitrary subnet to be passed in
cat <<EOF > /etc/network/interfaces
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address $THIS_NODE_IP
    netmask 255.255.255.0
    gateway $GATEWAY_IP
    hostname alpine39.localdomain
EOF

# set up DNS to hit google DNS
cat <<EOF > /etc/resolv.conf
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

# restart networking
/etc/init.d/networking restart

# sync up the system timezone with the actual clock
apk add tzdata
echo $TIMEZONE > /etc/TZ
rm /etc/localtime
cp /usr/share/zoneinfo/$TIMEZONE /etc/localtime
apk del tzdata
hwclock -s

# pre-configure (https://rancher.com/docs/k3s/latest/en/running/)
rc-update add cgroups default

sed -i.bak -e 's/^default_kernel_opts="\([^"]*\)"/default_kernel_opts="\1 cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory"/' /etc/update-extlinux.conf

# add IPv4 entry for localhost to /etc/hosts
echo "127.0.0.1 localhost localhost.localdomain" >> /etc/hosts

# add entry for master to /etc/hosts
echo "$MASTER_IP $MASTER_DNS" >> /etc/hosts

update-extlinux

# # ... and install k3s
curl -sfL https://get.k3s.io | K3S_URL=https://$MASTER_IP:6443 K3S_CLUSTER_SECRET=$SHARED_SECRET sh -

# move containerd config into the right place
chown root:root /home/vagrant/config.toml.tmpl
mkdir -p /var/lib/rancher/k3s/agent/etc/containerd
mv /home/vagrant/config.toml.tmpl /var/lib/rancher/k3s/agent/etc/containerd/

reboot
